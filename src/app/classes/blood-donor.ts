export class BloodDonor {
    _id: string;    // auto generated id by mongoDB
    bloodDonorUserName: string;
    bloodDonorFullName: string;
    bloodDonorEmail: string;
    bloodDonorPassword: string;
    bloodDonorNumber: string;
    bloodDonorLocation: string;
    bloodDonorBloodGroup: string;
}
