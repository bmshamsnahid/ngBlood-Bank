import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent} from 'ng-auto-complete';

export class BdDistricts {
    public group = [
        CreateNewAutocompleteGroup(
            'Location',
            'completer',
            [
                {title: 'Dhaka', id: '1'},
                {title: 'Rajshai', id: '2'},
                {title: 'Tangail', id: '3'},
                {title: 'Natore', id: '4'},
                {title: 'Chapai', id: '5'},
                {title: 'Barishal', id: '1'},
                {title: 'Ponchogor', id: '2'},
                {title: 'dfsdgsdOption 3', id: '3'},
                {title: 'ghrOpstion 4', id: '4'},
                {title: 'sdfOption 5', id: '5'},
            ],
            {titleKey: 'title', childrenKey: null}
        )];
}
