import { BloodDonor } from './../classes/blood-donor';
import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  private postUrl = 'https://node-blood-bank.herokuapp.com/auth/authenticate';
  private postUrlCreateUser = 'https://node-blood-bank.herokuapp.com/bloodDonor';

  // private postUrl = 'http://localhost:8080/auth/authenticate';
  // private postUrlCreateUser = 'http://localhost:8080/bloodDonor';

  constructor(private http: Http) { }

  getToken(email, password) {
    const body = {
      'bloodDonorEmail': email,
      'bloodDonorPassword': password
    };


    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});

    return this.http.post(this.postUrl, JSON.stringify(body), options)
      .map((response: Response) => {
        return response.json();
      });
  }

  createUser(bloodDonor: BloodDonor) {
    console.log('In auth service');

    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});

    return this.http.post(this.postUrlCreateUser, bloodDonor, options)
      .map((response: Response) => {
        return response.json();
      });
  }

}
