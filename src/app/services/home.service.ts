import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {

  private getUrl = 'https://node-blood-bank.herokuapp.com/bloodDonor';
  // private getUrl = 'http://localhost:8080/bloodDonor';

  constructor(private http: Http) { }

  // have to remove this method, insted of this, we will use getBloodDonors
  getUsers() {

    const headers = new Headers({'x-access-token': localStorage.getItem('myToken')});
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.getUrl, options)
      .map((response: Response) => {
        console.log(response);
        return response.json();
      });
  }

  getBloodDonors() {

    const headers = new Headers({'x-access-token': localStorage.getItem('myToken')});
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.getUrl, options)
      .map((response: Response) => {
        console.log(response);
        return response.json();
      });
  }

}
