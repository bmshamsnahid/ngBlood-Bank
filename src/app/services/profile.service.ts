import { BloodDonor } from './../classes/blood-donor';
import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ProfileService {

  private getUrl = 'https://node-blood-bank.herokuapp.com/bloodDonor';
  private putUrl = 'https://node-blood-bank.herokuapp.com/bloodDonor';
  // private getUrl = 'http://localhost:8080/bloodDonor';
  // private putUrl = 'http://localhost:8080/bloodDonor';

  constructor(private http: Http) { }

  getProfile() {
    const headers = new Headers({'x-access-token': localStorage.getItem('myToken')});
    const options = new RequestOptions({headers: headers});
    const profileId = localStorage.getItem('profileId');

    return this.http.get(this.getUrl + '/' + profileId, options)
      .map((response: Response) => {
        return response.json();
    });
  }

  updateProfile(bloodDonor: BloodDonor) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('x-access-token', localStorage.getItem('myToken'));
    // const headers = new Headers({'x-access-token': localStorage.getItem('myToken')});
    const options = new RequestOptions({headers: headers});
    const profileId = localStorage.getItem('profileId');

    console.log('Blood Donor Info');
    console.log(bloodDonor);

    return this.http.put(this.putUrl + '/' + profileId, JSON.stringify(bloodDonor), options)
      .map((response: Response) => {
        return response.json();
    });
  }

}
