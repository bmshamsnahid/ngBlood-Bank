import { BdDistricts } from './../../classes/bd-districts';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BloodDonor } from '../../classes/blood-donor';
import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent} from 'ng-auto-complete';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  desiredUserName: string;
  desiredFullName: string;
  desiredEmail: string;
  desiredPhoneNumber: string;
  desiredBloodGroup: string;
  desiredLocation: string;
  desiredPassword: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  bdDistricts: BdDistricts = new BdDistricts();

  public group;

  bloodGroups: string[] = [
    'A+',
    'A-',
    'B+',
    'B-',
    'AB+',
    'AB-',
    'O+',
    'O-'
  ];

  animal: string;
  name: string;

  hide = true;
  bloodDonor: BloodDonor = new BloodDonor();
  confirmBloodDonorPassword: string;
  newDonorInfo = new BloodDonor();

  constructor(private authService: AuthService,
              private router: Router) {
    this.group = this.bdDistricts.group;
  }

  ngOnInit() {
  }

  onClickSignUp() {

    this.newDonorInfo.bloodDonorUserName = this.desiredUserName;
    this.newDonorInfo.bloodDonorFullName = this.desiredFullName;
    this.newDonorInfo.bloodDonorEmail = this.desiredEmail;
    this.newDonorInfo.bloodDonorPassword = this.desiredPassword;
    this.newDonorInfo.bloodDonorNumber = this.desiredPhoneNumber;
    this.newDonorInfo.bloodDonorLocation = this.desiredLocation;
    this.newDonorInfo.bloodDonorBloodGroup = this.desiredBloodGroup;

    if (this.checkFormValidation() === true) {
      this.authService.createUser(this.newDonorInfo).subscribe((response) => {
        this.router.navigate(['/sign-in']);
      });
    } else {
      console.log('Error in form validation');
    }

  }

  Selected(item: SelectedAutocompleteItem) {
    this.desiredLocation = item.item.title;
    // console.log(item.item.title);
  }

  checkFormValidation() {

    console.log(this.desiredUserName);
    console.log(this.desiredFullName);
    console.log(this.desiredEmail);
    console.log(this.desiredPhoneNumber);
    console.log(this.desiredLocation);
    console.log(this.desiredBloodGroup);
    console.log(this.desiredPassword);

    if ((typeof this.desiredUserName === 'undefined') ||
        (this.desiredUserName.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredFullName === 'undefined') ||
        (this.desiredFullName.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredEmail === 'undefined') ||
        (this.desiredEmail.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredPhoneNumber === 'undefined') ||
        (this.desiredPhoneNumber.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredPassword === 'undefined') ||
        (this.desiredPassword.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredLocation === 'undefined') ||
        (this.desiredLocation.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredBloodGroup === 'undefined') ||
        (this.desiredBloodGroup.length <= 0)) {
          return false;
    }

    if (this.desiredPassword !== this.confirmBloodDonorPassword) {
      return false;
    }

    return true;
  }

}
