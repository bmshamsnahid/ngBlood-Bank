import { Subject } from 'rxjs/Subject';
import { AppStateService } from './../../services/app-state.service';
import { Component, OnInit } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  closeResult: string;
  isLoggedIn: boolean;

  constructor(private modalService: NgbModal,
              private appState: AppStateService,
              private router: Router) {
                console.log('Toke  Lenght: ' + localStorage.getItem('myToken').length);
                if (localStorage.getItem('myToken').length <= 0) {
                  this.isLoggedIn = false;
                } else {
                  this.isLoggedIn = true;
                }
  }

  ngOnInit() {
    this.appState.event.subscribe((data: String) => {
      if (data === 'true') {
        console.log('Success');
        this.isLoggedIn = true;
      } else {
        console.log('Not success');
        this.isLoggedIn = false;
      }
    });

  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickLogOut() {
    console.log('Is Logged In' + this.isLoggedIn);
    localStorage.setItem('myToken', '');
    localStorage.setItem('profileId', '');
    this.router.navigate(['/sign-in']);
    let loginStatus: any;
    loginStatus = 'false';
    this.appState.publish(loginStatus);
  }

}
