import { HomeService } from './../../services/home.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private users: any;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getUsers().subscribe((response) => {
      this.users = response;
      console.log(response);
    });
  }
}
