import { DonorDetailDialogComponent } from './../../donor-detail-dialog/donor-detail-dialog.component';
import { BloodDonor } from './../../../classes/blood-donor';
import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { HomeService } from '../../../services/home.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {

  animal: string;
  name: string;

  key = 'bloodDonorUserName'; // set default for searching
  reverse = false;  // sorting feature
  p = 1; // the pagination initial number

  bloodDonors: BloodDonor[];

  constructor(private homeService: HomeService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.homeService.getBloodDonors().subscribe((response) => {
      this.bloodDonors = response;
    });
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  openDialog(bloodDonor: BloodDonor): void {
    const dialogRef = this.dialog.open(DonorDetailDialogComponent, {
      width: '250px',
      data: bloodDonor
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

}
