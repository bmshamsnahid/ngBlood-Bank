import { BdDistricts } from './../../../classes/bd-districts';
import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

import { DonorDetailDialogComponent } from './../../donor-detail-dialog/donor-detail-dialog.component';

import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent} from 'ng-auto-complete';
import { HomeService } from '../../../services/home.service';
import { BloodDonor } from '../../../classes/blood-donor';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  desiredUserName: string;
  desiredFullName: string;
  desiredEmail: string;
  desiredPhoneNumber: string;
  desiredBloodGroup: string;
  desiredLocation: string;

  searchResult = false;

  @ViewChild(NgAutocompleteComponent) public completer: NgAutocompleteComponent;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  bdDistricts: BdDistricts = new BdDistricts();

  public group;

  bloodGroups: string[] = [
    'A+',
    'A-',
    'B+',
    'B-',
    'AB+',
    'AB-',
    'O+',
    'O-'
  ];

  bloodDonors = [];
  desiredBloodDonors = [];

  animal: string;
  name: string;

  key = 'bloodDonorUserName'; // set default for searching
  reverse = false;  // sorting feature
  p = 1; // the pagination initial number

  constructor(private homeService: HomeService,
              public dialog: MatDialog) {
    this.group = this.bdDistricts.group;
  }

  ngOnInit() {  }

  Selected(item: SelectedAutocompleteItem) {
    this.desiredLocation = item.item.title;
    // console.log(item.item.title);
  }

  onClickSearchDonor() {
    console.log('UserName: ' + this.desiredUserName);
    console.log('FullName: ' + this.desiredFullName);
    console.log('Email: ' + this.desiredEmail);
    console.log('Phone: ' + this.desiredPhoneNumber);
    console.log('Blood Group: ' + this.desiredBloodGroup);
    console.log('Location: ' + this.desiredLocation);

    this.homeService.getBloodDonors().subscribe((response) => {
      this.bloodDonors = response;

      this.desiredBloodDonors = [];

      for (const bloodDonor of this.bloodDonors) {
        if ((bloodDonor.bloodDonorUserName.includes(this.desiredFullName)) &&
            (this.desiredFullName.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        } else if ((bloodDonor.bloodDonorFullName.includes(this.desiredFullName)) &&
                    (this.desiredFullName.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        } else if ((bloodDonor.bloodDonorEmail.includes(this.desiredEmail)) &&
                    (this.desiredEmail.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        } else if ((bloodDonor.bloodDonorNumber.includes(this.desiredPhoneNumber)) &&
                  (this.desiredPhoneNumber.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        } else if ((bloodDonor.bloodDonorBloodGroup.includes(this.desiredBloodGroup)) &&
                  (this.desiredBloodGroup.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        } else if ((bloodDonor.bloodDonorLocation.includes(this.desiredLocation)) &&
                  (this.desiredLocation.length > 0)) {
          this.desiredBloodDonors.push(bloodDonor);
          continue;
        }
      }

      console.log('After filtering');
      console.log(this.desiredBloodDonors);
      if (this.desiredBloodDonors.length > 0) {
        this.searchResult = true;
      }
    });
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  openDialog(bloodDonor: BloodDonor): void {
    const dialogRef = this.dialog.open(DonorDetailDialogComponent, {
      width: '250px',
      data: bloodDonor
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

}
