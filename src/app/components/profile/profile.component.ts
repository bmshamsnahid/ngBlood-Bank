import { BdDistricts } from './../../classes/bd-districts';
import { BloodDonor } from './../../classes/blood-donor';
import { ProfileService } from './../../services/profile.service';
import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutocompleteComponent} from 'ng-auto-complete';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  desiredUserName: string;
  desiredFullName: string;
  desiredEmail: string;
  desiredPhoneNumber: string;
  desiredBloodGroup: string;
  desiredLocation: string;
  desiredPassword: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  bdDistricts: BdDistricts = new BdDistricts();

  public group;

  bloodGroups: string[] = [
    'A+',
    'A-',
    'B+',
    'B-',
    'AB+',
    'AB-',
    'O+',
    'O-'
  ];

  animal: string;
  name: string;

  hide = true;
  bloodDonor: BloodDonor = new BloodDonor();
  confirmBloodDonorPassword: string;
  newDonorInfo = new BloodDonor();

  constructor(private profileService: ProfileService) {
    this.group = this.bdDistricts.group;
  }

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this.profileService.getProfile().subscribe((response) => {
      this.bloodDonor = response[0];
      this.desiredUserName = this.bloodDonor.bloodDonorUserName;
      this.desiredFullName = this.bloodDonor.bloodDonorFullName;
      this.desiredEmail = this.bloodDonor.bloodDonorEmail;
      this.desiredPhoneNumber = this.bloodDonor.bloodDonorNumber;
      this.desiredBloodGroup = this.bloodDonor.bloodDonorBloodGroup;
      this.desiredLocation = this.bloodDonor.bloodDonorLocation;
      this.desiredPassword = this.bloodDonor.bloodDonorPassword;
    });
  }

  onClickUpdateProfile() {

    this.newDonorInfo.bloodDonorUserName = this.desiredUserName;
    this.newDonorInfo.bloodDonorFullName = this.desiredFullName;
    this.newDonorInfo.bloodDonorEmail = this.desiredEmail;
    this.newDonorInfo.bloodDonorPassword = this.desiredPassword;
    this.newDonorInfo.bloodDonorNumber = this.desiredPhoneNumber;
    this.newDonorInfo.bloodDonorLocation = this.desiredLocation;
    this.newDonorInfo.bloodDonorBloodGroup = this.desiredBloodGroup;

    if (this.checkFormValidation() === true) {
      this.profileService.updateProfile(this.newDonorInfo).subscribe((response) => {
        console.log(response);
      });
    } else {
      console.log('Error in form validation');
    }
  }

  Selected(item: SelectedAutocompleteItem) {
    this.newDonorInfo.bloodDonorLocation = item.item.title;
    // console.log(item.item.title);
  }

  checkFormValidation() {

    if ((typeof this.desiredUserName === 'undefined') ||
        (this.desiredUserName.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredFullName === 'undefined') ||
        (this.desiredFullName.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredEmail === 'undefined') ||
        (this.desiredEmail.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredPhoneNumber === 'undefined') ||
        (this.desiredPhoneNumber.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredPassword === 'undefined') ||
        (this.desiredPassword.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredLocation === 'undefined') ||
        (this.desiredLocation.length <= 0)) {
          return false;
    }

    if ((typeof this.desiredBloodGroup === 'undefined') ||
        (this.desiredBloodGroup.length <= 0)) {
          return false;
    }

    if (this.desiredPassword !== this.confirmBloodDonorPassword) {
      return false;
    }

    return true;
  }

}
