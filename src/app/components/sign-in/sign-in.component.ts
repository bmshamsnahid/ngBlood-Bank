import { AppStateService } from './../../services/app-state.service';
import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  hide = true;
  bloodDonorEmail = new FormControl('', [Validators.required, Validators.email]);
  bloodDonorPassword = new FormControl();

  getErrorMessage() {
    return this.bloodDonorEmail.hasError('required') ? 'You must enter a value' :
        this.bloodDonorEmail.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private authSerivce: AuthService,
              private router: Router,
              private appState: AppStateService) { }

  ngOnInit() {
  }

  onClickSignIn() {
    this.authSerivce.getToken(this.bloodDonorEmail.value, this.bloodDonorPassword.value).subscribe((response) => {
      if (response.success) {
        localStorage.setItem('myToken', response.token);
        localStorage.setItem('profileId', response.id);

        let loginStatus: any;
        loginStatus = 'true';

        this.appState.publish(loginStatus);

        this.router.navigate(['/home']);
      } else {
        console.log('Failed to get the token.');
      }
    });
  }

}
