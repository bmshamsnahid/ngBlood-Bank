import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BloodDonor } from '../../classes/blood-donor';

@Component({
  selector: 'app-donor-detail-dialog',
  templateUrl: './donor-detail-dialog.component.html',
  styleUrls: ['./donor-detail-dialog.component.css']
})
export class DonorDetailDialogComponent {

  bloodDonor: BloodDonor = new BloodDonor();

  constructor(
    public dialogRef: MatDialogRef<DonorDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.bloodDonor = data;
     }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
